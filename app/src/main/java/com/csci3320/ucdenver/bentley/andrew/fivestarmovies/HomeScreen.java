package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.ListView;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class HomeScreen extends ActionBarActivity {

    private Button button;
    TmdbApi api;
    public static final String key = "67cd1347bb48f15873543a67cfcc90c6";
    public static final long millisecondsInDay = 86400000;
    public static final String KEY = "/now_playing_cache";
    public static final String TIMEFILE = "/now_playing_timestamp";
    private ListView nowPlayingView;
    private MovieListAdapter adapter;
    boolean isRequestCached = false;
    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        button = (Button) findViewById(R.id.btnSearch);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Search.class);
                startActivity(i);
            }
        });


        nowPlayingView = (ListView) findViewById(R.id.homePageResults);

        pDialog = new ProgressDialog(this);
        // Set progressbar title
        pDialog.setTitle("Talking to TheMovieDB");
        // Set progressbar message
        pDialog.setMessage("Populating data...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();
        //since getting the api key and requesting data from themoviedb requires network
        //resources, don't do this on the main thread.
        new GetApiTask().execute();
        new LoadNowPlaying().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //create a new API object
    private class GetApiTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            api = new TmdbApi(key);
            return null;
        }
    }

    //Load the currently playing movies asynchronously and off of the main UI thread or else
    //android will throw a NetworkOnMainThread Exception but it will be buried in a stack trace
    //so you won't know what the hell is actually going on. -_-
    private class LoadNowPlaying extends AsyncTask<Void, Void, ArrayList<MovieDb>> {

        @Override
        protected ArrayList<MovieDb> doInBackground(Void... params) {

            //get the movies and load them in a List. We want to use an arraylist specifically
            //so transfer results once you get it.
            ArrayList<MovieDb> nowPlaying = null;

            //try and load the nowPlaying list from the cache file, if more than 24 hours have
            //gone by this should load from an API call
            try {
                nowPlaying = loadFromCache();
            }
            catch(ClassNotFoundException | IOException a) {
                a.printStackTrace();

                if (nowPlaying == null) {
                    MovieResultsPage results = api.getMovies().getNowPlayingMovies("en", 1);
                    nowPlaying = new ArrayList<>(results.getResults().size());
                    Iterator<MovieDb> itr = results.iterator();
                    while (itr.hasNext()) {
                        MovieDb movie = itr.next();
                        nowPlaying.add(movie);

                    }

                }
            }

            try {
                //write the timestamp
                PrintWriter writer = new PrintWriter(getFilesDir() + TIMEFILE, "UTF-8");
                writer.println(System.currentTimeMillis());
                writer.close();
                MyUtils.writeObject(HomeScreen.this, getFilesDir() + KEY, nowPlaying);
            }
            catch(IOException e) {
                System.out.println("Unable to open cache file for writing.");
                e.printStackTrace();
            }
            return nowPlaying;
        }

        protected ArrayList<MovieDb> loadFromCache() throws FileNotFoundException, IOException,
                ClassNotFoundException{

            ArrayList<MovieDb> cache = null;
            //try and open the file. If that fails return null. If the file has no
            //values return null, and if it has been more than 24 hours since
            //we've checked return null
            File timeStamp = new File(getFilesDir() + TIMEFILE);
            BufferedReader br = new BufferedReader(new FileReader(timeStamp));
            String s = br.readLine();
            if(s != null) {
                long oldRequest = Long.parseLong(s);
                long diff = System.currentTimeMillis() - oldRequest;
                if(diff > millisecondsInDay)
                    return null;

                cache = (ArrayList<MovieDb>) MyUtils.readObject(HomeScreen.this, getFilesDir() + KEY);
            }
            return cache;
        }

        //once that's finished, the adapter can be populated, which will in turn
        //populate the textKEY
        protected void onPostExecute(ArrayList<MovieDb> result) {

            pDialog.dismiss();
            if(result != null) {
                Collections.sort(result, new VoteCountCompare());
                adapter = new MovieListAdapter(HomeScreen.this,
                        android.R.layout.simple_list_item_1, api, result);

                nowPlayingView.setAdapter(adapter);
            }
            else {
                System.out.print("Resulting list is empty.");
            }
        }
    }

}