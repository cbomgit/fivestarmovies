package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by christian on 4/26/15.
 */
public class MyUtils {

    public static final long millisecondsInDay = 86400000;

    private MyUtils() {}


    //methods to read and write objects to/from cache files
    public static Object readObject(Context context, String key) throws IOException,
            ClassNotFoundException {
        FileInputStream fis = new FileInputStream(new File(key));
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        ois.close();
        fis.close();
        return object;
    }

    public static void writeObject(Context context, String key, Object object) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(key));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }
}
