package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.Utils;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.config.TmdbConfiguration;

//this causes the heap to run out of memory and doesn't place the thumbnails in the
//ImageView. Will revisit again later.
public class ThumbnailFetcher extends AsyncTask<Void, Void, Bitmap> {

    ImageView imageView;
    MovieDb movie;
    TmdbApi api;


    public ThumbnailFetcher(ImageView view, MovieDb m, TmdbApi a) {
        imageView = view;
        movie = m;
        api = a;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {

        TmdbConfiguration configuration = api.getConfiguration();
        List<String> posterSizes = configuration.getPosterSizes();


        String imageUrl = Utils.createImageUrl(api, movie.getPosterPath(), "w92").toString();
        Bitmap bitmap = null;

        if(imageUrl == null)
            this.cancel(true);

        try {
            bitmap  = BitmapFactory.decodeStream((InputStream) new URL(imageUrl).getContent());

        }
        catch(MalformedURLException e) {
            System.out.println(e);
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imageView.setImageBitmap(bitmap);
    }
}
