package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.view.Menu;
import android.view.MenuItem;

//This code can be used to get what value is selected on the spinners
//Spinner mySpinner=(Spinner) findViewById(R.id.your_spinner);
//String text = mySpinner.getSelectedItem().toString();

public class Search extends ActionBarActivity {

    //button to start next activity
    Button button;

    //radio buttons for refining search results
    RadioButton yearButton;
    RadioButton monthButton;
    RadioGroup group;

    //include months in our searches?
    boolean byYearOnly;

    //ui stuff
    Spinner dropdown1;
    Spinner dropdown2;
    ArrayAdapter<String> adapter1;
    ArrayAdapter<String> adapter2;

    //intent stuff
    String month, year;
    public static final String SEARCH_METHOD = "search_method";
    public static final String SEARCH_YEAR   = "search_year";
    public static final String SEARCH_MONTH  = "search_month";

    //might need these arrays elsewhere so make them static
    public static final String[] itemsYears = new String[]{"2015", "2014", "2013", "2012", "2011",
                                                           "2010", "2009", "2008", "2007", "2006",
                                                           "2005","2004", "2003", "2002", "2001",
                                                           "2000", "1999", "1998", "1997", "1996",
                                                           "1995", "1994", "1993", "1992", "1991",
                                                           "1990","1989", "1988", "1987", "1986",
                                                           "1985", "1984", "1983", "1982", "1981",
                                                           "1980", "1979", "1978", "1977", "1976",
                                                           "1975","1974", "1973", "1972", "1971",
                                                           "1970", "1969", "1968", "1967", "1966",
                                                           "1965", "1964", "1963", "1962", "1961",
                                                           "1960"};

    public static final String[] itemsMonth = new String[] {"January", "February", "March", "April",
                                                            "May", "June", "July", "August",
                                                            "September", "October", "November",
                                                            "December"};

    public static final String[] monthsNumerical = {"01", "02", "03", "04", "05", "06", "07", "08",
                                                    "09", "10", "11", "12"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        group = (RadioGroup) findViewById(R.id.searchGroup);
        yearButton = (RadioButton) findViewById(R.id.yearRbtn);
        monthButton = (RadioButton) findViewById(R.id.monthRbtn);
        byYearOnly = true;

        //default values in case nothing is selected
        year = "2015";
        month = "01";

        dropdown1 = (Spinner)findViewById(R.id.yearSpin);
        adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, itemsYears);
        dropdown1.setAdapter(adapter1);

        dropdown2 = (Spinner)findViewById(R.id.monthSpin);
        adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, itemsMonth);


        button = (Button) findViewById(R.id.btnToSearch);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SearchResults.class);
                i.putExtra(SEARCH_METHOD,byYearOnly);
                i.putExtra(SEARCH_YEAR,dropdown1.getSelectedItem().toString());
                if(dropdown2.getAdapter() != null) {
                    i.putExtra(SEARCH_MONTH, dropdown2.getSelectedItem().toString());
                }

                startActivity(i);
            }
        });

        //disable the month spinner if the year radio button is checked and enable it
        //otherwise
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.monthRbtn) {
                    byYearOnly = false;
                    dropdown2.setAdapter(adapter2);
                }
                else if(checkedId == R.id.yearRbtn) {
                    byYearOnly = true;
                    dropdown2.setAdapter(null);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
