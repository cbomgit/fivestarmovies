package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import java.util.Comparator;

import info.movito.themoviedbapi.model.MovieDb;

/**
 * Created by christian on 4/26/15.
 */

/* Some Comparator objects that can compare MovieDb objects either by
   voteCount or release date.
 */
public class VoteCountCompare implements Comparator<MovieDb> {

    @Override
    public int compare(MovieDb lhs, MovieDb rhs) {

        return rhs.getVoteCount() - lhs.getVoteCount();
    }
}


