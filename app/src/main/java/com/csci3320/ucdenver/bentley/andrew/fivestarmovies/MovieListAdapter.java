package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
;
import java.util.ArrayList;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;


/**
 * Created by christian on 4/26/15.
 *
 * MovieListAdapter displays a list of movie items in a ListView. This way an arbitrary number
 * of items can be shown without having to worry about hardcoding that value in. The adapter
 * knows about the data that should populate the view and knows how to format each view based
 * on the view's id.
 */
public class MovieListAdapter extends ArrayAdapter<MovieDb> {

    ArrayList<MovieDb> movieList;   //list of movies retrieved from API
    LayoutInflater inflater;
    Context context;                //calling activity
    TmdbApi api;       //config used to build Image url

    public MovieListAdapter(Context c, int textViewResourceId,
                            TmdbApi a, ArrayList<MovieDb> objects) {

        super(c, textViewResourceId, objects);

        movieList = objects;
        context = c;
        inflater = LayoutInflater.from(context);
        api = a;
    }

    public int getCount() {
        return movieList.size();
    }

    public MovieDb getItem(int position) {
        return movieList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder; //hold the widgets for the item view

        //if a view already exists, just get the tag and assign the correct values to holder
        //otherwise, fill everything in
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_layout,parent,false);
            holder.movieTitle = (TextView) convertView.findViewById(R.id.txt_movie_title);
            holder.movieRating = (TextView) convertView.findViewById(R.id.txt_movie_rating);
            holder.releaseDate = (TextView) convertView.findViewById(R.id.txt_release_date);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(movieList != null) {
            MovieDb movie = movieList.get(position);

            if(movie != null) {

                if(holder != null) {
                    if(holder.movieTitle != null)
                        holder.movieTitle.setText("" + movie.getOriginalTitle());
                    if(holder.movieRating != null)
                        holder.movieRating.setText("" + movie.getVoteCount());
                    if(holder.releaseDate != null)
                        holder.releaseDate.setText("" + movie.getReleaseDate());


                        //holder.thumb.setImageDrawable(thumbs.get(position));
                }
                else {
                    System.out.println("Holder never instantiated");
                }

            }
        }
        return convertView;
    }



    /*This lets us bundle all the movie info together and tag it
      in case the array adapter ever calls getView with existing data
     */
    private class ViewHolder {
        TextView movieTitle;
        TextView movieRating;
        TextView releaseDate;
        ImageView thumb;

        public ViewHolder() {
            movieTitle = new TextView(context);
            movieRating = new TextView(context);
            releaseDate = new TextView(context);
            thumb = new ImageView(context);
        }
    }
}
