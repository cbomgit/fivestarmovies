package com.csci3320.ucdenver.bentley.andrew.fivestarmovies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.Discover;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;


public class SearchResults extends ActionBarActivity {

    public static final String key = "67cd1347bb48f15873543a67cfcc90c6";
    Button button1, button2;
    ListView searchResultsView;
    private String KEY = "/top_rated_cache";
    private String TIMEFILE = "/top_rated_timestamp";
    TmdbApi api;
    MovieListAdapter adapter;
    ProgressDialog pDialog;
    boolean byYearOnly;
    String year, month;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        year = "2015";      //set defaults just in case?
        month = "01";

        Bundle extras = getIntent().getExtras();
        //unwrap the intent from the last activity
        byYearOnly = extras.getBoolean(Search.SEARCH_METHOD);
        year = extras.getString(Search.SEARCH_YEAR);
        month = extras.getString(Search.SEARCH_MONTH);
        //construct our cache names since we don't want to use the same cache for
        //every search
        KEY = "/results_cache_" + year;
        TIMEFILE = "/timestamp_" + year;
        if(!byYearOnly) {
            KEY = KEY + "_" + month;
            TIMEFILE = TIMEFILE + "_" +month;
        }

        setContentView(R.layout.activity_search_results);

        //show some progress
        pDialog = new ProgressDialog(this);
        // Set progressbar title
        pDialog.setTitle("Talking to TheMovieDB");
        // Set progressbar message
        pDialog.setMessage("Populating data...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();

        button1 = (Button) findViewById(R.id.btnBackHome);
        button1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HomeScreen.class);
                startActivity(i);
            }
        });

        button2 = (Button) findViewById(R.id.btnSearchAgain);
        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Search.class);
                startActivity(i);
            }
        });

        searchResultsView = (ListView) findViewById(R.id.searchResultsView);
        new LoadPopularMovies().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //Load the currently playing movies asynchronously and off of the main UI thread or else
    //android will throw a NetworkOnMainThread Exception but it will be buried in a stack trace
    //so you won't know what the hell is actually going on. -_-
    private class LoadPopularMovies extends AsyncTask<Void, Void, ArrayList<MovieDb>> {

        @Override
        protected ArrayList<MovieDb> doInBackground(Void... params) {

            //get the movies and load them in a List. We want to use an arraylist specifically
            //so transfer results once you get it.
            ArrayList<MovieDb> popularMovies = null;
            api = new TmdbApi(HomeScreen.key);
            //try and load the popularMovies list from the cache file, if more than 24 hours have
            //gone by this should load from an API call
            try {
                popularMovies = loadFromCache();
            }
            catch(ClassNotFoundException | IOException a) {
                a.printStackTrace();
                Discover discover;

                if (popularMovies == null) {
                    if(byYearOnly) {
                        discover = configDiscover(year);
                    }
                    else {
                        discover = configDiscover(year, month);
                    }
                    MovieResultsPage results = api.getDiscover().getDiscover(discover);
                    popularMovies = new ArrayList<>(results.getResults().size());
                    Iterator<MovieDb> itr = results.iterator();
                    while (itr.hasNext()) {
                        MovieDb movie = itr.next();
                        popularMovies.add(movie);

                    }

                }
            }

            try {
                //write the timestamp
                PrintWriter writer = new PrintWriter(getFilesDir() + TIMEFILE, "UTF-8");
                writer.println(System.currentTimeMillis());
                writer.close();
                MyUtils.writeObject(SearchResults.this, getFilesDir() + KEY, popularMovies);
            }
            catch(IOException e) {
                System.out.println("Unable to open cache file for writing.");
                e.printStackTrace();
            }
            return popularMovies;
        }

        protected ArrayList<MovieDb> loadFromCache() throws FileNotFoundException, IOException,
                ClassNotFoundException{

            ArrayList<MovieDb> cache = null;
            //try and open the file. If that fails return null. If the file has no
            //values return null, and if it has been more than 24 hours since
            //we've checked return null
            File timeStamp = new File(getFilesDir() + TIMEFILE);
            BufferedReader br = new BufferedReader(new FileReader(timeStamp));
            String s = br.readLine();
            if(s != null) {
                long oldRequest = Long.parseLong(s);
                long diff = System.currentTimeMillis() - oldRequest;
                if(diff > MyUtils.millisecondsInDay)
                    return null;

                cache = (ArrayList<MovieDb>) MyUtils.readObject(SearchResults.this,getFilesDir() + KEY);
            }
            return cache;
        }

        //once that's finished, the adapter can be populated, which will in turn
        //populate the textKEY
        protected void onPostExecute(ArrayList<MovieDb> result) {

            pDialog.dismiss();
            if(result != null) {
                adapter = new MovieListAdapter(SearchResults.this,
                        android.R.layout.simple_list_item_1, api, result);

                searchResultsView.setAdapter(adapter);
            }
            else {
                System.out.print("Resulting list is empty.");
            }
        }
    }

    //Discover objects filter search results. Overloaded method for searching by year
    //and by year and month
    private Discover configDiscover(String year) {
        Discover discover = new Discover();
        discover.page(1);
        discover.primaryReleaseYear(Integer.parseInt(year));
        discover.sortBy("vote_count.desc");
        discover.language("en");
        return discover;
    }

    private Discover configDiscover(String year, String month) {
        Discover discover = new Discover();
        String date = year + "-" + month + "-01";
        discover.page(1);
        discover.releaseDateGte(date);
        discover.sortBy("vote_count.desc");
        return discover;
    }

}
